var i;
function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var txtNickname = document.getElementById('inputNickname');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()" and clean input field
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(success) {
            console.log("Login success 1.");
            window.location.href = "index.html";
        }).catch(function(error) {
            // Handle Errors here.
            create_alert("error", error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    btnGoogle.addEventListener('click', function() {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()"
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            console.log(user.email);
            var added_email = user.email.replace(/\./g, '_');
            console.log(added_email);
            var ref = firebase.database().ref('users/' + added_email);

            ref.once("value").then(function(snapshot) {
                if(snapshot.exists()){
                    window.location.href = "index.html";
                }
                else{
                    writeUserData(added_email, user.displayName).then(function() {
                        console.log("Wrote user data.");
                        console.log("Login success 2.");
                        window.location.href = "index.html";
                    }, function(error) {
                        console.log(error.message);
                    });
                }
            });
          }).catch(function(error) {
            // Handle Errors here.
            create_alert("error", error.message);
          });    
    });

    btnSignUp.addEventListener('click', function() {
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value).then(function() {
            create_alert("success", "Sign up success.");
            console.log("sign up success.");

            writeUserData(txtEmail.value.replace(/\./g, '_'), txtNickname.value).then(function() {
                console.log("Wrote user data.");
            
                txtEmail.value = "";
                txtPassword.value = "";
                txtNickname.value = "";
            }, function(error) {
                console.log(error.message);
            });
        }).catch(function(error) {
            // Handle Errors here.
            create_alert("error", error.message);
            console.log(error);

            txtEmail.value = "";
            txtPassword.value = "";
            txtNickname.value = "";
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

function writeUserData(email, nickname) {
    console.log(nickname);
    if(!nickname) {
        nickname = email;
        console.log(nickname);
    }
    return firebase.database().ref('users/' + email).set({
        nickname: nickname,
        photo : "img/user.png",
        email: email
    });
}

window.onload = function() {
    initApp();
};