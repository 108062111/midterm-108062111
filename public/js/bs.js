var user_name = 'Romi', user_email = '';
var chatting_name = '', chatting_photo = '' , chatting_email = '', chatting_key = '';
var usersRef = firebase.database().ref('users');
var chatRef = firebase.database().ref('chat');
var first_count_f = 0, second_count_f = 0;
var i;
function init() {

    firebase.auth().onAuthStateChanged(function(user) {
        // Check user login
        if (user) {
            user_email = user.email.replace(/\./g, '_');
            usersRef.child(user_email).once("value")
                .then(function(snapshot) {
                    var title = document.getElementById('title');
                    var myprofile = document.getElementById('myprofile');
                    var myphoto = document.getElementById('myphoto');
                    var myname = document.getElementById('myname');
                    var myemail = document.getElementById('myemail');
                    user_name = snapshot.child("nickname").val();
                    title.innerHTML = '<b>' + user_name + "'s Chat Room</b>";
                    document.getElementById('chatting-title').innerHTML = '';
                    myphoto.src = snapshot.child("photo").val();
                    myname.innerText = user_name;
                    myemail.innerText = user_email;
                    myprofile.addEventListener('click', async function() {
                        chatting_name = '';
                        chatting_photo = '';
                        chatting_email = '';
                        document.getElementById('chatting-title').innerHTML = '';
                        await removechatting_key();
                        updateMessage();
                    });
                });
            var logout_btn = document.getElementById('logout-btn');
            // var settings_btn = document.getElementById('settings-btn');
            logout_btn.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    console.log("Sign out success.");
                }).catch(function(error) {
                    console.log(error.message);
                });
            });
            // settings_btn.addEventListener('click', function() {
            //     // TODO
            // });
        } else {
            window.location.href = "signin.html";
        }
    });

    // create new chat room
    var add_btn = document.getElementById('adduser-btn');
    var added_user = document.getElementById('added-user');

    var friends = document.getElementById('friends');

    add_btn.addEventListener('click', function() {
        if (added_user.value) {
            var added_nickname = '', added_photo = '';
            var added_email = added_user.value.replace(/\./g, '_');
            var chat_room_exists = false;

            usersRef.once("value")
            .then(function(snapshot) {
                if(snapshot.child(added_email).exists()){
                    console.log('added_email exists.');
                    added_nickname = snapshot.child(added_email + '/nickname').val();
                    added_photo = snapshot.child(added_email + '/photo').val();

                    chatRef.once('value')
                        .then(async function(snapshot) {
                            await snapshot.forEach(function(childshot) {
                                if(childshot.child('users/' + user_email).exists() &&
                                childshot.child('users/' + added_email).exists()){
                                    chat_room_exists = true;
                                    console.log('chat_room exists.');
                                    return;
                                }
                            });
                            if(!chat_room_exists){
                                chatRef.push({
                                    users:{
                                        [user_email]: user_email,
                                        [added_email]: added_email
                                    }
                                }).then( function(){
                                    console.log("push chat room.");
                                });
                                
                                /*var htmlString = '<div class="user">' +
                                    '<img src="' + added_photo + '">' +
                                '</div>' +
                                '<p class="name-time">' +
                                    '<span class="name">' + added_nickname + '</span>' +
                                    ' <span class="time">' + added_email + '</span>' +
                                '</p>';

                                var tmp = document.createElement('li');
                                tmp.className = "person";
                                tmp.id = added_email;
                                tmp.innerHTML = htmlString;
                                friends.appendChild(tmp);
                                console.log("append html");

                                tmp.addEventListener('click', function() {
                                    chatting_name = this.children[1].children[0].textContent;
                                    chatting_photo = this.children[0].children[0].src;
                                    chatting_email = this.children[1].children[1].textContent;
                                    console.log(chatting_name + ' : ' + chatting_email);
                                    chatting_key = '';
                                    updateMessage();
                                });

                                first_count_f += 1;*/
                            }
                    });
                }
                else{
                    console.log(added_email + " doesn't exists.");
                }
            });
            added_user.value = "";
        }
    });

    // post
    var post_btn = document.getElementById('post_btn');
    var post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', async function() {
        if (post_txt.value != "") {
            var post_value = post_txt.value.toString().replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&#39;").replace(/"/g, "&#34;");
            if(!chatting_key){
                await chatRef.once('value')
                    .then( function(snapshot) {
                        snapshot.forEach(function(childshot) {
                            console.log(childshot.val());
                            if(childshot.child('users/' + user_email).exists() &&
                            childshot.child('users/' + chatting_email).exists()){
                                console.log('find room');
                                chatting_key = childshot.key;
                                return;
                            }
                        });
                    });
            }
            chatRef.child(chatting_key + '/messages').push({
                ts: new Date().toLocaleString(),
                usr: user_email,
                msg: post_value
            })
            .then(function(){
                console.log("Push success.");
            });
            post_txt.value = "";
        }
    });


    // left friend box

    chatRef.once('value').then(function(snapshot) {
        snapshot.forEach(function(childshot) {
            if(childshot.child('users/' + user_email).exists()){
                first_count_f += 1;
                // console.log(childshot.val());
                childshot.child('users').forEach(function(grandchildshot) {
                    var childData = grandchildshot.val();
                    if(childData == user_email) return;

                    var f_nickname = '', f_photo = '';
                    usersRef.child(childData).once('value').then(function(snapshot){
                        f_nickname = snapshot.child('nickname').val();
                        f_photo = snapshot.child('photo').val();
                        // console.log('left friend: ' + f_nickname);

                        var htmlString = '<div class="user">' +
                            '<img src="' + f_photo + '">' +
                            '<span class="name"> ' + f_nickname + '</span>' +
                        '</div>' +
                        '<p class="name-time">' +
                            ' <span class="time"> ' + childData + '</span>' +
                        '</p>';

                        var tmp = document.createElement('li');
                        tmp.className = "person";
                        tmp.id = childData;
                        tmp.innerHTML = htmlString;
                        friends.appendChild(tmp);
                        // console.log("append html 2");

                        tmp.addEventListener('click', async function() {
                            if(chatting_email == this.children[1].children[0].textContent.slice(1)) return;
                            chatting_name = this.children[0].children[1].textContent;
                            chatting_photo = this.children[0].children[0].src;
                            chatting_email = this.children[1].children[0].textContent.slice(1);
                            document.getElementById('chatting-title').innerHTML = f_nickname;
                            console.log(chatting_name + ' : ' + chatting_email);
                            await removechatting_key();
                            updateMessage();
                        });
                    })
                    .catch(e => console.log(e.message));
                });
            }
        });
        /// Add listener to update new chat room
        chatRef.on('child_added', function(childshot) {
            if(childshot.child('users/' + user_email).exists()){
                second_count_f += 1;
                // console.log(childshot.key);
                if(second_count_f > first_count_f){
                    childshot.child('users').forEach(function(grandchildshot) {
                        var childData = grandchildshot.val();
                        if(childData == user_email) return;
    
                        var f_nickname = '', f_photo = '';
                        usersRef.child(childData).once('value').then(function(snapshot){
                            f_nickname = snapshot.child('nickname').val();
                            f_photo = snapshot.child('photo').val();
                            console.log('left friend 3: ' + f_nickname);
                        
                            var htmlString = '<div class="user">' +
                                '<img src="' + f_photo + '">' +
                                '<span class="name"> ' + f_nickname + '</span>' +
                            '</div>' +
                            '<p class="name-time">' +
                                ' <span class="time"> ' + childData + '</span>' +
                            '</p>';

                            var tmp = document.createElement('li');
                            tmp.className = "person";
                            tmp.id = childData;
                            tmp.innerHTML = htmlString;
                            friends.appendChild(tmp);
                            // console.log("append html 3");

                            tmp.addEventListener('click', async function() {
                                if(chatting_email == this.children[1].children[0].textContent.slice(1)) return;
                                chatting_name = this.children[0].children[1].textContent;
                                chatting_photo = this.children[0].children[0].src;
                                chatting_email = this.children[1].children[0].textContent.slice(1);
                                document.getElementById('chatting-title').innerHTML = f_nickname;
                                console.log(chatting_name + ' : ' + chatting_email);
                                await removechatting_key();
                                updateMessage();
                            });
                        });
                    });
                }
            }
        });
    })
    .catch(e => console.log(e.message));
}


function removechatting_key(){
    if(chatting_key){
        chatRef.child(chatting_key + '/messages').off('child_added');
    }
    chatting_key = '';
}

function updateMessage(){
    console.log('show msg: ', chatting_email);
    if(!chatting_email) {
        document.getElementById('post_list').innerHTML = '';
        return;
    }
    chatRef.once('value').then(function(snapshot) {
        // List for store posts html
        var total_post = [];
        // Counter for checking history post update complete
        var first_count = 0;
        // Counter for checking when to update new post
        var second_count = 0;

        snapshot.forEach(function(childshot) {
            if(childshot.child('users/' + user_email).exists() &&
            childshot.child('users/' + chatting_email).exists()){
                chatting_key = childshot.key;
                // console.log("chatting_key: ", chatting_key);

                childshot.child('messages').forEach(function(grandchildshot) {
                    var grandchildshot = grandchildshot.val();
                    var htmlString;
                    // console.log(grandchildshot.usr);
                    // console.log(user_email);
                    if(grandchildshot.usr == user_email){
                        htmlString = '<li class="chat-right">' +
                        '<div class="chat-hour">' + grandchildshot.ts + '</div>' +
                        '<div class="chat-text">' + grandchildshot.msg + '</div>' +
                            '<div class="chat-avatar">' +
                                '<img src="' + chatting_photo + '" alt="Retail Admin">' +
                                '<div class="chat-name">' + user_name + '</div>' +
                            '</div>' +
                        '</li>';
                    }
                    else{
                        htmlString = '<li class="chat-left">' +
                            '<div class="chat-avatar">' +
                                '<img src="' + chatting_photo + '" alt="Retail Admin">' +
                                '<div class="chat-name">' + chatting_name + '</div>' +
                            '</div>' +
                            '<div class="chat-text">' + grandchildshot.msg + '</div>' +
                            '<div class="chat-hour">' + grandchildshot.ts + '</div>' +
                        '</li>';
                    }
                    total_post[total_post.length] = htmlString;
                    first_count += 1
                });
                return;
            }
        });

        /// Join all post in list to html in once
        document.getElementById('post_list').innerHTML = total_post.join('');

        /// Add listener to update new post
        chatRef.child(chatting_key + '/messages').on('child_added', function(data) {
            second_count += 1;
            if (second_count > first_count) {
                var grandchildshot = data.val();
                var htmlString;
                if(grandchildshot.usr == user_email){
                    htmlString = '<li class="chat-right">' +
                    '<div class="chat-hour">' + grandchildshot.ts + '</div>' +
                    '<div class="chat-text">' + grandchildshot.msg + '</div>' +
                        '<div class="chat-avatar">' +
                            '<img src="' + chatting_photo + '" alt="Retail Admin">' +
                            '<div class="chat-name">' + user_name + '</div>' +
                        '</div>' +
                    '</li>';
                }
                else{
                    htmlString = '<li class="chat-left">' +
                        '<div class="chat-avatar">' +
                            '<img src="' + chatting_photo + '" alt="Retail Admin">' +
                            '<div class="chat-name">' + chatting_name + '</div>' +
                        '</div>' +
                        '<div class="chat-text">' + grandchildshot.msg + '</div>' +
                        '<div class="chat-hour">' + grandchildshot.ts + '</div>' +
                    '</li>';
                    mynotify(chatting_photo, grandchildshot.msg);
                }
                total_post[total_post.length] = htmlString;
                document.getElementById('post_list').innerHTML = total_post.join('');
            }
        });
    })
    .catch(e => console.log(e.message));
}

window.onload = function() {
    init();
};

function mynotify (img, text) {
    console.log(text);
    // At first, let's check if we have permission for notification
    // If not, let's ask for it
    if (Notification && Notification.permission !== "granted") {
      Notification.requestPermission(function (status) {
        if (Notification.permission !== status) {
          Notification.permission = status;
        }
      });
    }
    // If the user agreed to get notified
    // Let's try to send ten notifications
    if (Notification && Notification.permission === "granted") {
        var n = new Notification("Chat Room", { body: text, icon: img });
    }
    // If the user hasn't told if he wants to be notified or not
    // Note: because of Chrome, we are not sure the permission property
    // is set, therefore it's unsafe to check for the "default" value.
    else if (Notification && Notification.permission !== "denied") {
    Notification.requestPermission(function (status) {
        if (Notification.permission !== status) {
        Notification.permission = status;
        }
        // If the user said okay
        if (status === "granted") {
            var n = new Notification("Chat Room", { body: text, icon: img });
        }
        // Otherwise, we can fallback to a regular modal alert
        else {
        alert("You haven't agreed to receive notification.");
        }
    });
    }
    // If the user refuses to get notified
    else {
    // We can fallback to a regular modal alert
        alert("You haven't agreed to receive notification.");
    }
}