# Software Studio 2021 Spring Midterm Project

## Topic
* Project Name : midterm-108062111

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://midterm-108062111.web.app

## Website Detail Description

When you enter the page, if you have not signed in, then the page will redirect to the sign in page.

After sign in, you will enter your chat room.

The left side shows your friends, whom you can chat with. If you want to add a new friend, just type your friend's email on the top input box and click the add-user button next to it.

The right side shows who yor are chatting with, and all the messages between you and your friends. If you want to send message, type your message in the down input box and click the paper airplane button next to it.


# Components Description : 

Sign in page:

1. Sign in : Type your email and password to sign in.

2. Sign in with Google : A pop-up window will occur. Choose your google account to sign in. If you are sign in with Google first time, then it will write the user data into the database.

3. New Account: Type your email, password, and nickname to sign up. If you didn't type your nickname, then your nickname will be set as your email. It will write the user data into the database.


Chatroom page:

(On the top navbar)

4. Account : Click and you will see the logout button.

5. Logout : Click and you will logout.

(On the left division)

6. Email input box : Type your friend's email on the top input box and click the add-user button next to it. If your friend has registered, then it will create a chatroom for you and your friend.

7. Row of you and your friends' profiles : Click and you will enter the chatroom with him/her. If you click your profile, then you will see an empty chatroom.

(On the right division)

8. Messages : You can see all the messages between you and your friends.

9. Message input box : Type your message in the down input box and click the paper airplane button next to it.


# Other Functions Description : 

1. Chrome Notification : First, you have to approve to receive the notification from chrome. Then you will receive the notification from your friend who you are chatting with.

2. Use CSS Animation : On sign in page, the background is gradient animation, and the both sides have three rectangles fly through.

3. Security Report : Every message will be used "replace()" to replace <, >, ', " with their html reference code and then put into the database, so it can display the messages that contain html codes correctly.

